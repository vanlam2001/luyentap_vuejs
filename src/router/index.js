import { createRouter, createWebHistory } from 'vue-router';
import Layout from '../Layout/Layout.vue'
import HomePage from '../HomePage/HomePage.vue'

const routes = [
    {
        path: '/',
        component: Layout,
        props: {
            Component: HomePage,
        },
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;

